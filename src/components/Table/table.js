import React, { Component } from 'react'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import axios from 'axios';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { Button } from '@mui/material';



export default class BasicTable extends Component {
    state = {
        vehicles : [],
        msg:""
     }
     componentDidMount() {
        this.getVehicles()
      }

    getVehicles = async() => {
        
        
        const api_call = await fetch(`https://63037d409eb72a839d824580.mockapi.io/Vehicle`)
        const data = await api_call.json()
        
        this.setState({
            vehicles: data,
        })
        console.log(this.state.vehicles);

    }


    DeleteVehicles = (ppp) => {
        console.log("fontion working");
      axios.delete(`https://63037d409eb72a839d824580.mockapi.io/Vehicle/${ppp}`)
      .then((response) => {
         console.log("deleted", response)
      }).catch((e) => {
          console.log("error", e);
      })

  }
     

  render() {
    const {vehicles} = this.state

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell align="right">Manufacturer</TableCell>
            <TableCell align="right">Model</TableCell>
            <TableCell align="right">Fuel</TableCell>
            <TableCell align="right">Color</TableCell>
            <TableCell align="right">Price</TableCell>
            <TableCell align="right">Currency</TableCell>
            <TableCell align="right">City</TableCell>
            <TableCell align="right">Country</TableCell>
            <TableCell align="right">Delete</TableCell>
            
            </TableRow>
        </TableHead>
        <TableBody>
          {vehicles.map((item) => (
            <TableRow
              key={item.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {item.name}
              </TableCell>
              <TableCell align="right">{item.manufacturer}</TableCell>
              <TableCell align="right">{item.model}</TableCell>
              <TableCell align="right">{item.fuel}</TableCell>
              <TableCell align="right">{item.color}</TableCell>
              <TableCell align="right">{item.price}</TableCell>
              <TableCell align="right">{item.currency}</TableCell>
              <TableCell align="right">{item.city}</TableCell>
              <TableCell align="right">{item.country}</TableCell>
              <TableCell align="right"> <Button onClick={()=>{this.DeleteVehicles(item.id)}}>
              <DeleteForeverIcon  sx={{alignItems:"center", color :"red" ,ml:3}}/></Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
}
