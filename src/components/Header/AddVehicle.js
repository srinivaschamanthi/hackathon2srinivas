import React, { useState ,useEffect} from "react";
// import "./AddVehicle.scss";
import axios from "axios"
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { Button, Typography } from "@mui/material";



export default function AddVehicle() {
  const [name,setName] = useState("");
  const [manufacturer,setManufacturer]=useState("");
  const [model,setModel]=useState("");
  const [fuel,setFuel]=useState("");
  const [color,setColor]=useState("");
  const [price,setPrice]=useState("");
  const [currency,setCurrency]=useState("");
  const [city,setCity]=useState("");
  const [country,setCountry]=useState("");
  const [post,setPost]=useState("");
  const[msg,setMsg]=useState("")
  const[data,setData]=useState({
    name:"",
    manufacturer:"",
      model:"",
      fuel:"",
      color:"",
      price:"",
      currency:"",
      city:"",
      country:"",
      
  })
  useEffect(() => {
    axios.get('https://63037d409eb72a839d824580.mockapi.io/Vehicle')
        .then((response) => {
            setPost(response.data);
        })
}, [])

 
 
  const AddPost = () => {
    console.log("data", data)
    const postData = {
        
      id: (post.length + 1),
      name:data.name,
      manufacturer:data.manufacturer,
      model:data.model,
      fuel:data.fuel,
      color:data.color,
      price:data.price,
      currency:data.currency,
      city:data.city,
      country:data.country
    } 
    // if(name===""||manufacturer===""||model===""||fuel===""||color===""||price===""||currency===""||city===""||country===""){
    //       setMsg("fill all fields")
    // } else {
      console.log("working pradeep");
      axios.post('https://63037d409eb72a839d824580.mockapi.io/Vehicle', postData)
      .then((response) => {
          console.log("successfully add")
          setData({
            name:"",
            manufacturer:"",
            model:"",
            fuel:"",
            color:"",
            price:"",
            currency:"",
            city:"",
            country:""
          })
          setMsg("Add successfully")
      }).catch(() => {
          setMsg("error")
      })
    // }

   
}
  

  return (
    <div className="App">
      

      
        <div clasName="modal-container">
       <Box sx={{display:"flex", flexDirection:"column",justifyContent:"end"}}>
        <Typography variant="h6" sx={{ml:2}}>Add a Vehicle</Typography>
        <Typography variant="h10" sx={{ml:2}}>all fields are mandatory</Typography>

          
       
         <Box
      component="form"
      sx={{
        '& > :not(style)': { mt: 2, ml:2, width: '90%' ,},
      }}
      noValidate
      autoComplete="off"
    > 


    <TextField id="outlined-basic" type="text"  sx={{mb:1}}
    value={data.name}
    onChange={(e)=>setData({...data,name:e.target.value})}  label="Name" variant="outlined" />

      <TextField id="outlined-basic" type="text"
           value={data.manufacturer}
           onChange={(e)=>setData({...data,manufacturer:e.target.value})} label="Manfacturer" variant="outlined" />
      <TextField id="outlined-basic" type="text"
           value={data.model}
           onChange={(e)=>setData({...data,model:e.target.value})} label="Model" variant="outlined" />
      <TextField id="outlined-basic" type="text"
           value={data.fuel}
           onChange={(e)=>setData({...data,fuel:e.target.value})} label="Fuel" variant="outlined" />
      <TextField id="outlined-basic" type="text"
           value={data.color}
           onChange={(e)=>setData({...data,color:e.target.value})} label="Color" variant="outlined" />
      <TextField id="outlined-basic" type="text"
           value={data.price}
           onChange={(e)=>setData({...data,price:e.target.value})} label="Price" variant="outlined" />
      <TextField id="outlined-basic" type="text"
           value={data.currency}
           onChange={(e)=>setData({...data,currency:e.target.value})} label="Currency" variant="outlined" />
      <TextField id="outlined-basic" type="text"
           value={data.city}
           onChange={(e)=>setData({...data,city:e.target.value})} label="City" variant="outlined" />
      <TextField id="outlined-basic" type="text"
           value={data.country}
           onChange={(e)=>setData({...data,country:e.target.value})} label="Country" variant="outlined" />
      
    </Box>
    </Box>
    <Typography sx={{color:"red"}}>{msg}</Typography>
        </div>
        <div style={{marginTop:"20px",marginBottom:"10px"}}>
        <Button onClick={AddPost} sx= {{ml:2}} variant="contained">+ Add Vehicle</Button>
        {/* <button onClick={toggleModal} style={{color:"blue",marginLeft:"10px"}}>Cancel</button> */}
        </div>
    </div>
    
    
  
  );

}