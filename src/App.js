import React from 'react'

import Header from './components/Header/header';
import Table from './components/Table/table';
import Home from './components/Home/home';

const  App = () => {
  
  return (
    <>
      <Header/>
      <Home/>
      
    </>
  )
}
export default App;
